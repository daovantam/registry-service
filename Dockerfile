FROM openjdk:8
ADD target/registry-service.jar registry-service.jar
EXPOSE 8761
ENTRYPOINT ["java", "-jar", "registry-service.jar"]